import paramiko
import pytest

"""Role testing files using testinfra."""

# def AnsibleVars(host):
#     all_vars = host.ansible.get_variables()
#     return all_vars

# #@pytest.fixture
# def test_get_vars(host):
#     defaults_files = "file=defaults/main.yml name=role_defaults"
#     ansible_vars = host.ansible(
#         "include_vars",
#         defaults_files)["ansible_facts"]["role_defaults"]
#     return ansible_vars

def prepare_ssh_connection(host):
    host_info = host.run("tail -1 /etc/hosts").stdout
    host_ip = host_info.split()[0]

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    
    return ssh, host_ip

def test_fail_ssh_connection_with_bad_user(host):
    ssh, host_ip = prepare_ssh_connection(host)
    try:
        ssh.connect(host_ip, username="baduser", key_filename='molecule/default/tests/ssh_key/test_rsa')
    except Exception:
        assert True

def test_fail_ssh_connection_without_key(host):
    ssh, host_ip = prepare_ssh_connection(host)
    try:
        ssh.connect(host_ip, username="user")
    except Exception:
        assert True

def test_success_ssh_connection(host):
    ssh, host_ip = prepare_ssh_connection(host)
    try:
        ssh.connect(host_ip, username="user", key_filename='molecule/default/tests/ssh_key/test_rsa')
    except Exception:
        assert False

@pytest.mark.parametrize('pattern', [
    b'PermitRootLogin no',
    b'PasswordAuthentication no'
])  
def test_success_sshd_config_content(host, pattern):
    content = host.file('/etc/ssh/sshd_config').content
    assert pattern in content

@pytest.mark.parametrize('pattern', [
    b'PermitRootLogin yes',
    b'PasswordAuthentication yes',
    b'PermitRootLogin without-password'
])  
def test_fail_sshd_config_content(host, pattern):
    content = host.file('/etc/ssh/sshd_config').content
    assert pattern not in content